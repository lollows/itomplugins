# coding=iso-8859-15
'''

By twipOS 2014.
'''
from itom import userIsDeveloper
from itom import userIsAdmin
from itom import dataObject
from itom import ui
from itomUi import ItomUi
import inspect
import os.path
import numpy as np
from itom import filter
from numpy import NaN

hasBASICFILTERS = True
if not (itom.pluginLoaded("BasicFilters")):
    loadWarnings.append("- Not all functions available since plugin 'BasicFilters' not available.")
    hasBASICFILTERS = False

class histogramEditor(ItomUi):
    
    workingDir = ""
    startDir = ""
    upDating = True
    measureType = 0
    plotElementType = 0
    haseMCPP = False
    selectedDObj = None
    #pickedPointsPhys = dataObject()
    #colTVec = []
    colTVec = dataObject.zeros([256], dtype='float64')
    imageOrig = dataObject()
    imageMod = dataObject()    
    
    elementCount = 0    
    enumGeomNames = ["invalid", "multipoint", "point", "line", "rectangle", "square", "ellipse", "circle", "polygon"]
    enumGeomIdx = [0, 1, 2, 4, 8, 16, 32, 64, 128]
    enumRelationName = None
    
    def __init__(self, hasBASICFILTERS, systemPath = None):
        self.startDir = os.getcwd()
        self.upDating = True
        self.measureType = 0
        self.hasMCPP = hasBASICFILTERS
        
        if(systemPath is None):
            ownFilename = inspect.getfile(inspect.currentframe())
            ownDir = os.path.dirname(os.path.realpath(__file__)) #os.path.dirname(ownFilename)
        else:
            ownDir = systemPath
        
        uiFile = os.path.join(ownDir, "ui/histogramEditor.ui")
        uiFile = os.path.abspath(uiFile)
        ItomUi.__init__(self, uiFile, ui.TYPEDIALOG, childOfMainWindow=True)
        
        self.workingDir = self.startDir
        self.fillInf = True
        self.hasObject = False
        self.filename = ""
        self.evalObj = dataObject()
        self.pickedPointsPix = dataObject.zeros([2, 2], 'float32')
        
    def init(self, skipBox, defaultVarName):
        self.updateDObj(skipBox, defaultVarName)
        
        self.upDating = False
        return

    def getVarNameDialog(self, title, VarName, workSpace, objType = 'ND', acceptedObjectType = 1):
        '''
        getVarName(title, VarName, workSpace [, objType]) -> opens a drop down window with suitable DataObjects / npOjects / PointClouds and returns selected variable
        
        Parameters 
        ------------
        title : {str}
            Title of the dialog.
        VarName :  {str}
            default variable name.
        VarName :  {PythonDict}
            Kontent of the global workspace of caller (use globals())
        objType : {str}, optinal 
            ObjectTypes to filter. Can be ' line', 'plane' , ' empty' or 'ND'
        acceptedObjectType: {int}, optional
            BitSet 1 = DataObjects, 2 = NumPy-Array, 4 = PointClouds accepted
        
        
        Returns
        -------
        varname: {str}
            string value of the variable
        done:  {bool}
            Check, if dialog was closed with 'ok' or 'cancel'
        
        Notes
        -------
        This function opens a drop down window with suitable DataObject according to 'objType'.
        The selected Object-Name is returned together with a boolean expression representing
        the dialog status. If only one element fits to 'objType', the dialog is skipped.
        '''
        
        dObjects = []
        npObjects = []
        pcObjects = []
        
        key = ""
        objtype = 1
        dimsTestString = ''
        dimsNPTestString = ''
        
        try:
            if (acceptedObjectType & 2) == 2:
                eval("numpy.ndarray")
        except:
            import numpy
        
        try:
            if (acceptedObjectType & 4) == 4:
                eval("itom.PointCloud")
        except:
            acceptedObjectType = acceptedObjectType - 4
        
        if (acceptedObjectType & 1) == 1:
            if objType == 'empty':
                dimsTestString = '{}.dims == 0'
            elif objType == 'plane':
                dimsTestString = '{}.shape[{}.dims-1]>1 and {}.shape[{}.dims-2]>1'
            elif objType == 'line':
                dimsTestString = '{}.shape[{}.dims-1] == 1 or {}.shape[{}.dims-2] == 1'
            elif objType == 'lineORplane':
                dimsTestString = '{}.ndim > 0 and ({}.ndim < 3 or (({}.ndim < 4) and ({}.shape[0] == 1)))'
            else:
                dimsTestString = '{}.dims > 0'
                
        if (acceptedObjectType & 2) == 2:
            if objType == 'empty':
                dimsNPTestString = '{}.ndim == 0'
            elif objType == 'plane':
                dimsNPTestString = '{}.ndim > 1 and {}.shape[{}.ndim-1]>1 and {}.shape[{}.ndim-2]>1'
            elif objType == 'line':
                dimsNPTestString = '({}.ndim == 1) or ({}.shape[{}.ndim-1] == 1) or ({}.shape[{}.ndim-2] == 1)'
            elif objType == 'lineORplane':
                dimsNPTestString = '{}.ndim > 0 and ({}.ndim < 3 or (({}.ndim < 4) and ({}.shape[0] == 1)))'
            else:
                dimsNPTestString = '{}.ndim > 0'        
        
        for key in workSpace.keys():
            try:
                if ((acceptedObjectType & 1) == 1) and (type(workSpace[key]) == itom.dataObject):
                    try:
                        if eval(dimsTestString.format(key, key, key, key), workSpace):
                            dObjects.append(key)
                    except:
                        temp = 0
                        del temp
                        
                if ((acceptedObjectType & 2) == 2) and (type(workSpace[key]) == numpy.ndarray or type(workSpace[key]) == itom.npDataObject):
                    try:
                        if eval(dimsNPTestString.format(key, key, key, key, key), workSpace):
                            npObjects.append(key)
                    except:
                        temp = 0
                        del temp
            except:
                temp = 0
                del temp
        
        if (len(npObjects) < 1) and (len(pcObjects) < 1) and (len(dObjects) < 1):
            ui.msgCritical("Plot", "No dataObjects found", ui.MsgBoxOk)
            varname = ""
            done = False
            objtype = 0
            
        elif (len(npObjects) < 1) and (len(pcObjects) < 1):
            objtype = 1
            if len(dObjects) > 1:
                objindex = [i for i,x in enumerate(dObjects) if x == VarName]
                
                if len(objindex) > 0:
                    objindex = objindex[0]
                else:
                    objindex = 0
                
                [varname, done]= itom.ui.getItem(title, "DataObjects", dObjects, objindex, False)
            else:
                varname = dObjects[0]
                done = True

        elif (len(dObjects) < 1) and (len(pcObjects) < 1):
            objtype = 2
            if len(npObjects) > 1:
                objindex = [i for i,x in enumerate(npObjects) if x == VarName]
                
                if len(objindex) > 0:
                    objindex = objindex[0]
                else:
                    objindex = 0
                
                [varname, done]= itom.ui.getItem(title, "numpyArrays", npObjects, objindex, False)
            else:
                varname = npObjects[0]
                done = True
        else:
            allDics = []
            allDics.extend(dObjects)
            allDics.extend(npObjects)
            allDics.extend(pcObjects)
            
            objindex = [i for i,x in enumerate(allDics) if x == VarName]
            
            if len(objindex) > 0:
                objindex = objindex[0]
            else:
                objindex = 0
            
            [varname, done]= itom.ui.getItem(title, "numpyArrays", allDics, objindex, False)
            
            if varname in pcObjects:
                objtype = 4
            elif varname in npObjects:
                objtype = 2            
            else:
                objtype = 1

            del allDics
        
        del key
        del dObjects
        del npObjects
        del pcObjects
        del dimsTestString
        del dimsNPTestString
        
        return [varname, objtype, done]

    def updateDObj(self, skipBox, defaultVarName):
        if self.hasMCPP == False:
            ui.msgCritical("DataObject", "Execution error, MCPPFilterFuncs not loaded", ui.MsgBoxOk)
            check = False
            result = 0
            return [check, result]
        
        if defaultVarName == None:
            dataObj = self.defaultVarName
        else:
            dataObj = defaultVarName
        
        if skipBox == False:
            [dataObj, obtype, check] = self.getVarNameDialog("Choose Object:", dataObj, globals(), 'lineORplane', 1)
        else:
            check = True
        
        if check == True:
            try:
                # Check if variable exists
                eval(dataObj)
                self.selectedDObj = dataObj
            except:
                if skipBox == False:
                    ui.msgCritical("DataObject", "Variable does not exist", ui.MsgBoxOk)
                return [False, 0]
            try:
                self.imageOrig = eval(dataObj)
                #self.waveTxt = self.gui.waveTxt["html"]
        
                self.gui.image["source"] = self.imageOrig
                self.gui.image["colorMap"] = "gray"
                self.gui.image.setProperty({"title" : "Image"})
                self.gui.image["zAxisInterval"] = [0,255]
        
                tempHist = dataObject()
                s = "filter(\"calcHist\",{}, tempHist, bins=256)"
                eval(s.format(dataObj))
                self.histObj = tempHist
                tempHist2 = dataObject.zeros([tempHist.size()[0] + 1, tempHist.size()[1]], dtype=tempHist.dtype)
                tempHist2[0:tempHist.size()[0], :] = tempHist.copy()
                tempHist2[tempHist.size()[0], :] = dataObject(np.arange(0, 256))
                
                self.gui.histogram["source"] = tempHist2
                self.gui.histogram.setProperty({"title" : "Histogram"})
                self.gui.histogram.setProperty({"yAxisInterval" : [0, 255]})
                self.gui.histogram.setProperty({"xAxisInterval" : [0, 255]})
            except:
                if skipBox == False:
                    ui.msgCritical("DataObject", "Execution error", ui.MsgBoxOk)
                check = False
                result = 0
        else:
            result = 0

    def findNextPt(self, histoPts, lastX):
        nextX = 99999
        nextPt = -1
        nextPtIdx = -1
        for n in range(0, len(histoPts)):
            if histoPts[n].point1[0] > lastX and histoPts[n].point1[0] < nextX:
                nextPtIdx = histoPts[n].index
                nextPt= n
                nextX = histoPts[n].point1[0]
        return (nextPtIdx, histoPts[nextPt])

    def updateImage(self, colTVec, offset):
        self.imageMod = self.imageOrig.copy()
        if self.imageMod.dims == 2:
            for y in range(0, self.imageMod.shape[0]):
                for x in range(0, self.imageMod.shape[1]):
                    oldCol = self.imageMod[y, x]
                    if self.imageMod.dtype=='rgba32' or self.imageMod.dtype=='rgb32':
                        newCol = oldCol
                        newCol.r = (oldCol.r - offset) * colTVec[0, oldCol.r]
                        newCol.g = (oldCol.g - offset) * colTVec[0, oldCol.g]
                        newCol.b = (oldCol.b - offset) * colTVec[0, oldCol.b]
                        self.imageMod[y, x] = newCol
                    else:
                        self.imageMod[y, x] = np.floor((oldCol - offset) * colTVec[0, oldCol])
        elif self.dims == 3:
            for p in range(0, self.imageMod.shape[0]):
                for y in range(0, self.imageMod.shape[1]):
                    for x in range(0, self.imageMod.shape[2]):
                        oldCol = self.imageMod[p, y, x]
                        if self.imageMod.dtype=='rgba32' or self.imageMod.dtype=='rgb32':
                            newCol = oldCol
                            newCol.r = (oldCol.r - offset) * colTVec[0, oldCol.r]
                            newCol.g = (oldCol.g - offset) * colTVec[0, oldCol.g]
                            newCol.b = (oldCol.b - offset) * colTVec[0, oldCol.b]
                            self.imageMod[p, y, x] = newCol
                        else:
                            self.imageMod[p, y, x] = np.floor((oldCol - offset) * colTVec[0, oldCol])
        else:
            print("error image dimension must be 2 or 3 for histogram editor")
            
        self.gui.image["source"] = self.imageMod
        
        
    def updateHist(self, histoPts, newElementCount):
        #pts = dict()
        #firstPt = 999
        #for n in range(0, newElementCount):
            #if int(histoPts[n].index) < firstPt:
                #firstPt = int(histoPts[n].index)
            #pts.update({str(int(histoPts[n].index)) + "x" : histoPts[n].point1[0]})
            #pts.update({str(int(histoPts[n].index)) + "y" : histoPts[n].point1[1]})
        #if firstPt == 999:
            #firstPt = 0
        
        lastX = 0
        pt = 0
        tempHist2 = dataObject.zeros([self.histObj.size()[0] + 1, self.histObj.size()[1]], dtype=self.histObj.dtype)
        tempHist2[0 : self.histObj.size()[0], :] = self.histObj.copy()
        if newElementCount > 0:
            self.colTVec = dataObject.zeros([256], dtype='float64')
            for n in range(0, newElementCount + 1):
                lastPt = pt
                (thisPt, pt) = self.findNextPt(histoPts, lastX)
                lastX = pt.point1[0]
                if n == 0:
                    valX1 = 0
                    valY1 = 0
                    valX2 = pt.point1[0]
                    valY2 = pt.point1[1]
                    #valX2 = pts[str(int(firstPt + n)) + "x"]
                    #valY2 = pts[str(int(firstPt + n)) + "y"]
                elif n == newElementCount:
                    #valX1 = pts[str(int(firstPt + n - 1)) + "x"]
                    #valY1 = pts[str(int(firstPt + n - 1)) + "y"]
                    valX1 = lastPt.point1[0]
                    valY1 = lastPt.point1[1]
                    valX2 = 255
                    valY2 = 255
                else:
                    valX1 = lastPt.point1[0]
                    valY1 = lastPt.point1[1] 
                    valX2 = pt.point1[0]
                    valY2 = pt.point1[1]
                    #valX1 = pts[str(int(firstPt + n - 1)) + "x"]
                    #valY1 = pts[str(int(firstPt + n - 1)) + "y"]
                    #valX2 = pts[str(int(firstPt + n)) + "x"]
                    #valY2 = pts[str(int(firstPt + n)) + "y"]
                dx = np.ceil(valX2 - valX1)
                dy = (valY2 - valY1) / dx
                for p in range(0, int(dx), 1):
                    tempHist2[self.histObj.size()[0], p + int(valX1)] = int(valY1 + dy * p)
        else:    
            tempHist2[self.histObj.size()[0], :] = dataObject(np.arange(0, 256, 1))
            
        for col in range(0, 256):
            if col > 0:
                self.colTVec[0, col] = tempHist2[self.histObj.size()[0], col] / col
            else:
                self.colTVec[0, col] = tempHist2[self.histObj.size()[0], col]
        tempHist2[self.histObj.size()[0], 255] = 255
        self.gui.histogram["source"] = tempHist2
        self.updateImage(self.colTVec, 0)
        #del pts
        
    def show(self,modalLevel = 0):
        ret = self.gui.show(modalLevel)
    
    @ItomUi.autoslot("")
    def on_pushButtonCancel_clicked(self):
        self.gui.hide()
        global histEdit
        del histEdit
        return
    
    @ItomUi.autoslot("")
    def on_pushButtonAccept_clicked(self):
        exec("global " + self.selectedDObj + "\n" + self.selectedDObj + " = self.gui.image['source']")
        self.gui.hide()
        global histEdit
        del histEdit
        return
        
    @ItomUi.autoslot("")
    def on_pushButtonEqualize_clicked(self):
        lowerBound = 0
        upperBound = 255
        curHist = self.gui.histogram["source"]
        dims = curHist.dims
        for n in range(0, 256):
            if curHist[0, n] > 0:
                if n > 0:
                    lowerBound = n
                break
        for n in range(255, -1, -1):
            if curHist[0, n] > 0:
                if n < 255:
                    upperBound = n
                break
        spread = 255 / (upperBound - lowerBound)
        self.colTVec = dataObject.zeros([256], dtype='float64')
        for col in range(0, 256):
            self.colTVec[0, col] = spread
        if (spread != 1) or (lowerBound != 0):
            self.updateImage(self.colTVec, lowerBound)
            #self.updateHist(shape(), 0)
        return
        
    @ItomUi.autoslot("")
    def on_pushButtonAddPt_clicked(self):
        self.plotElementType = self.enumGeomIdx[self.enumGeomNames.index('point')]
        self.gui.histogram.call("userInteractionStart", self.plotElementType, True, 1)
        return

    @ItomUi.autoslot("")
    def on_pushButtonRemovePt_clicked(self):
        idx = self.gui.histogram["selectedGeometricShape"]
        if idx >= 0:
            self.gui.histogram.call("deleteGeometricShape", idx)

        histoPts = self.gui.histogram["geometricShapes"]
        newElementCount = self.gui.histogram["geometricShapesCount"]
        self.updateHist(histoPts, newElementCount)
        self.measureType = 0
        self.elementCount = newElementCount
        
        return
        
    @ItomUi.autoslot("int,ito::Shape")
    def on_histogram_geometricShapeChanged(self, idx, shape):
        histoPts = self.gui.histogram["geometricShapes"]
        newElementCount = self.gui.histogram["geometricShapesCount"]
        self.updateHist(histoPts, newElementCount)        
        return
        
    @ItomUi.autoslot("QVector<ito::Shape>,bool")
    def on_histogram_geometricShapeFinished(self, shapes, aborted):
        histoPts = self.gui.histogram["geometricShapes"]
        newElementCount = self.gui.histogram["geometricShapesCount"]
        
        if(self.elementCount + 1 > newElementCount):
            self.measureType = 0
            return
            
        self.updateHist(histoPts, newElementCount)
        #tempObj = self.gui.dataPlot["source"]
        #tempScales = tempObj.axisScales
        #tempOffsets = tempObj.axisOffsets 

        self.measureType = 0
        self.elementCount = newElementCount

if(__name__ == '__main__'):
    histEdit = histogramEditor(hasBASICFILTERS)
    histEdit.init(False, "")
    histEdit.show()
    #histEdit.gui.setAttribute(55, True)